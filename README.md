Christine Poulard, Céline Berni, INRAE  June 2021

Teaching tool to illustrate the influence of the rugosity and computation space-step on the estimation of a simple backwater curve, in a Wide rectangular channel.

The simplest script is ligne-eau-reservoir_vuCP, in french only.
For demo purposes "subcritical_flow_channel_into_reservoir.py" was developped : it is interactive, with sliders allowing to modify the values of roughness and discretisation step (left screenshot).
The figure displays the estimated backwater curve when possible. When the normal depth hn is under the critical depth hc or when it would make the water level at the reservoir entrance higher than the reservoir level, the computation is not possible with the hypotheses of the exercice : hc and hn are drawn but on a pink background  ( screenshot on the right)

It appeared that sliders are not suitable for the range of discretisation step, so the input is now through a textbox widget (right screenshot) 

| Code with sliders for K and delta_h | Code with sliders for K and TextBox for delta_h  |
| ------ | ------ |
| ![screenshot](/screenshots/Subcritical_backwater.PNG) | ![screenshot](/screenshots/BackwaterDemoTextBox_impossible.png) |
| <i>  with default values for K and delta_h : computation is possible</i> | <i> K and delta_h were changed ; K such as computation is _impossible (hn < hc)_ </i>  |


